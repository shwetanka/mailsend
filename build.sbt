name := "mailer"

version := "0.0.1"

organization := "com.onsurity"

scalaVersion := "2.12.10"

libraryDependencies ++= {
  Seq(
    "org.apache.commons" % "commons-email" % "1.5",
    "org.scala-lang" % "scala-xml" % "2.11.0-M4",
    "mysql" % "mysql-connector-java" % "5.1.12",
    "log4j" % "log4j" % "1.2.14"
  )
}

