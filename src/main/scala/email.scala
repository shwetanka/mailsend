import java.util.Properties
import org.apache.commons.mail._
import scala.io.Source
import scala.actors.Actor._

package object email {

  implicit def stringToSeq(single: String): Seq[String] = Seq(single)
  implicit def liftToOption[T](t: T): Option[T] = Option(t)

  def auth: Properties = {
    val prop = new Properties()
    prop.load(Source.fromURL(getClass.getResource("/mail.props")).bufferedReader())
    prop
  }

  sealed abstract class MailType
  case object Plain extends MailType
  case object Rich extends MailType
  case object MultiPart extends MailType

  case class Mail(from: (String, String), // (email, name)
                  to: Seq[String],
                  subject: String,
                  message: String,
                  cc: Seq[String] = Seq.empty,
                  bcc: Seq[String] = Seq.empty,
                  richMessage: Option[String] = None,
                  attachment: Option[(java.io.File)] = None
                  )

  object send {
    def a(mail: Mail) {
      val format =
        if (mail.attachment.isDefined) MultiPart
        else if (mail.richMessage.isDefined) Rich
        else Plain

      val commonsMail: Email = format match {
        case Plain => new SimpleEmail().setMsg(mail.message)
        case Rich => new HtmlEmail().setHtmlMsg(mail.richMessage.get)
        case MultiPart => {
          val attachment = new EmailAttachment()
          attachment.setPath(mail.attachment.get.getAbsolutePath)
          attachment.setDisposition(EmailAttachment.ATTACHMENT)
          attachment.setName(mail.attachment.get.getName)
          new MultiPartEmail().attach(attachment).setMsg(mail.message)
        }
      }

      commonsMail.setHostName(auth.getProperty("mail.host").getOrElse(""))
      commonsMail.setAuthentication(auth.getProperty("mail.username").getOrElse(""),
                                    auth.getProperty("mail.password").getOrElse(""))
      commonsMail.setTLS(true)
      commonsMail.setSmtpPort(auth.getProperty("mail.port").getOrElse("0").toInt)

      mail.to foreach (commonsMail.addTo(_))
      mail.cc foreach (commonsMail.addCc(_))
      mail.bcc foreach (commonsMail.addBcc(_))

      commonsMail.setFrom(mail.from._1, mail.from._2).setSubject(mail.subject).send()
    }
  }

  val mailSender = actor {
    while(true){
      receive {
        case mail: Mail => send a mail
      }
    }
  }
}