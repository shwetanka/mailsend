import email._
import scala.io.Source
import scala.xml.NodeSeq

object Mailer extends App {

  def htmlMsg(code: String): NodeSeq =
    <html>
      <body>
        Hello FreeCharger, <br/>
        <p>
          Looks like we messed up a bit and you received a bad FreeFund code for the Despicable Me 2 Facebook share offer. We
          sincerely apologize for this.
        </p>
        <p>
          Here's your new FreeFund code: {code}
        </p>
        <p>
          Go ahead and redeem your FreeFund code by going to <a href="https://www.freecharge.in/app/mybalance.htm">https://www.freecharge.in/app/mybalance.htm</a>
        </p>
        <br/>
        Thanks, <br/>
        Customer Support
        <br/><br/>
        ps: In case of any queries, please contact <a href="mailto:care@freecharge.com">care@freecharge.com</a>
      </body>
    </html>

  val emails = Source.fromURL(getClass.getResource("/emails.dat")).getLines()
  val coupons = Source.fromURL(getClass().getResource("/coupons.dat")).getLines()

  coupons.foreach{
    coupon =>
      if(emails.hasNext) mailSender ! new Mail(from = ("noreply@freecharge.com", "FreeCharge"),
                                              to = emails.next(),
                                              subject = "FreeCharge Freefund coupon",
                                              message = "",
                                              richMessage = htmlMsg(coupon).toString())
  }
}